angular.module('services', [])
  .factory('browser', ['$cordovaInAppBrowser', '$rootScope', function($cordovaInAppBrowser, $rootScope) {
    // For the purpose of this example I will store user data on ionic local storage but you should save it on a database
    var _event_handlers = null;

    function fire(name, event) {
      if (_event_handlers && typeof _event_handlers[name] == 'function') {
        _event_handlers[name](event);
      }
    }
    $rootScope.$on('$cordovaInAppBrowser:loadstart', function(e, event) {
      fire('loadstart', event);
    }); // Called when inAppBrowser start loading the page.
    $rootScope.$on('$cordovaInAppBrowser:loadstop', function(e, event) {
      fire('loadstop', event);
    }); // Called when inAppBrowser has finished loading the page.
    $rootScope.$on('$cordovaInAppBrowser:loaderror', function(e, event) {
      fire('loaderror', event);
    }); //Called when inAppBrowser has encountered error.
    $rootScope.$on('$cordovaInAppBrowser:exit', function(e, event) {
      fire('exit', event);
      _event_handlers = null;
    });

    return {
      executeScript: function(obj, cb) {
        $cordovaInAppBrowser.executeScript(obj).then(cb);
      },
      go: function(opts) {
        var options = {
          location: 'no',
          clearcache: 'yes',
          toolbar: 'no'
        };
        if (!opts.url)
          throw 'url must be passed to browser';
        _event_handlers = opts.handlers;
        $cordovaInAppBrowser.open(opts.url, '_blank', options)

        .then(function(event) {
          opts.callback(null, event);
        })

        .catch(function(event) {
          opts.callback(event);
        });
      },
      close: function() {
        $cordovaInAppBrowser.close();
      }
    };
  }])
  .factory('appDomain', function() {
    return new com_scheme_labs_411();
  })
  .factory('container', ['appDomain', 'browser', 'loading', 'nav', 'backend', 'diag', 'session', '$timeout', '$cordovaSocialSharing', 'cam', function(appDomain, browser, loading, nav, backend, diag, session, $timeout, $cordovaSocialSharing, cam) {
    var container = appDomain.container();
    container.add('browser', browser);
    container.add('loading', loading);
    container.add('nav', nav);
    container.add('backend', backend);
    container.add('diag', diag);
    container.add('session', session);
    container.add('login', appDomain.viewmodels.Login);
    container.add('registration', appDomain.viewmodels.Registration);
    container.add('forgotPassword', appDomain.viewmodels.ForgotPassword);
    container.add('changePassword', appDomain.viewmodels.ChangePassword);
    container.add('placeDetail', appDomain.viewmodels.PlaceDetail);
    container.add('home', appDomain.viewmodels.Home);
    container.add('notificationCenter', appDomain.viewmodels.NotificationCenter);
    container.add('clubRating', appDomain.viewmodels.ClubRating);
    container.add('messageBoard', appDomain.viewmodels.MessageBoard);
    container.add('dashboard', appDomain.viewmodels.Dashboard);
    container.add('profile', appDomain.viewmodels.Profile);
    container.add('socialSharing', $cordovaSocialSharing);
    container.add('searchForPlace', appDomain.viewmodels.SearchForPlace);
    container.add('newPlace', appDomain.viewmodels.NewPlace);
    container.add('cam', cam);
    container.add('timer', {
      get: function() {
        return $timeout;
      }
    });
    container.add('container', container);
    return container;
  }])
  .factory('utils', function() {
    return {
      getAssetDirectory: function() {
        return ionic.Platform.isAndroid() ? '/android_asset/www/' : '/';
      }
    };
  })
  .factory('loading', ['$ionicLoading', function($ionicLoading) {
    var busy = false;
    return {
      //hide the title.
      show: function(title, spinner) {
        busy = true;
        $ionicLoading.show({
          template: '<ion-spinner class=\'spinner-light\' icon=\'' + (spinner || 'android') + '\'  ></ion-spinner>'
        });
      },
      hide: function(id) {
        busy = false;
        $ionicLoading.hide();
      },
      isBusy: function() {
        return busy;
      }

    };
  }])
  .factory('diag', ['$ionicPopup', '$rootScope', '$ionicModal', '$ionicPopover', 'utils', '$cordovaToast', function($ionicPopup, $rootScope, $ionicModal, $ionicPopover, utils, $cordovaToast) {

    var _modals = [];

    function remove(m) {
      var index = 0;
      index = _modals.indexOf(m);
      if (index !== -1) {
        _modals.splice(index, 1);
      }
    }
    return {
      SHORT_DURATION: 'short',
      LONG_DURATION: 'long',
      CENTER_POSITION: 'center',
      BOTTOM_POSITION: 'bottom',
      TOP_POSITION: 'top',
      show: function(opt) {
        if (!opt.fromTemplate) {
          var alertPopup = $ionicPopup.alert({
            title: opt.title,
            template: opt.content,
            okType: 'button-outline no-border'
          });
          alertPopup.then(function() {
            remove(alertPopup);
            if (opt.cb)
              opt.cb();
          });
          _modals.push(alertPopup);
        } else {
          var scope = $rootScope.$new();
          scope.model = opt.content;
          scope.model.close = function() {
            _modal.hide();
          };
          var _modal = null;
          $ionicModal.fromTemplateUrl(opt.fromTemplate, {
            scope: scope,
            animation: opt.animation || 'scale-in',
            buttons: opt.buttons
          }).then(function(modal) {
            _modals.push(modal);
            _modal = modal;
            modal.show();
          });
          var ticket = scope.$on('modal.hidden', function() {
            remove(_modal);
            ticket();
            _modal.remove();
            if (opt.cb) {
              opt.cb();
            }
          });
          return;
        }

        //alertPopup.then(opt.cb);
      },
      popover: function(opt) {

        var templateUrl = utils.getAssetDirectory();
        switch (opt.name) {
          case 'dashboard':
            templateUrl = 'templates/dashboard-menu.html';
            break;
        }
        var scope = $rootScope.$new();
        var _popover = null;
        scope.model = opt.content;
        scope.model.popover$close = function() {
          _popover.hide();
        };
        $ionicPopover.fromTemplateUrl(templateUrl, {
          scope: scope
        }).then(function(popover) {
          _popover = popover;
          _modals.push(popover);
          _popover.show(opt.event);
        });
        var ticket = scope.$on('popover.hidden', function() {
          remove(_popover);
          ticket();
          _popover.remove();
          if (opt.cb) {
            opt.cb();
          }
        });
      },
      toast: function(message, duration, position) {
        $cordovaToast.show(message, duration, position);
      },
      confirm: function(opt) {
        var confirmPopup = $ionicPopup.confirm({
          title: opt.title,
          template: opt.content,
          cancelText: 'no',
          okType: 'button-balanced',
          cancelType: 'button-clear'
        });
        confirmPopup.then(function(res) {
          if (res) {
            if (opt.yes)
              opt.yes();
          } else {
            if (opt.no)
              opt.no();
          }
        });
      },
      closeAll: function() {
        _modals.forEach(function(item) {
          if (item.hide)
            item.hide();
          else
          if (item.close)
            item.close();
        });
      }

    };
  }])
  .factory('nav', ['$location', '$ionicHistory', '$state', '$ionicNativeTransitions', '$rootScope', function($location, $ionicHistory, $state, $ionicNativeTransitions, $rootScope) {

    return {
      go: function(path, disableBack, wipe) {

        if (disableBack)
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
        if (wipe) {
          $ionicHistory.clearHistory();
        }
        //$location.path(path);
        $ionicNativeTransitions.locationUrl(path);

      },
      back: function() {
        if ($ionicHistory.backView())
          $ionicHistory.goBack(-1);
        else
          navigator.app.exitApp();
      },
      goState: function(name) {
        $state.go(name);
        // $rootScope.$apply();
      }
    };
  }])
  .factory('cam', ['$cordovaCamera', function($cordovaCamera) {
    return {
      open: function() {
        var options = {
          destinationType: Camera.DestinationType.FILE_URI,
          sourceType: Camera.PictureSourceType.CAMERA,
        };

        var prom = $cordovaCamera.getPicture(options);
        return prom;
      },
      clean: function() {
        return $cordovaCamera.cleanup();
      }
    };

  }])
  .factory('backend', ['appDomain', '$http', 'Upload', function(appDomain, $http, upload) {
    return new appDomain.backend({
      request: function(opt) {
        if (opt) {
          var config = {};
          config.url = opt.url;
          config.method = opt.verb;
          config.headers = opt.headers;
          if (opt.verb == 'POST' || opt.verb == 'PUT') {
            if (opt.headers && opt.headers['Content-Type'] && opt.headers["Content-Type"] == "application/x-www-form-urlencoded") {
              var result = "";
              for (var key in opt.data) {
                if (opt.data.hasOwnProperty(key)) {
                  result += encodeURI(key) + "=" + encodeURI(opt.data[key]) + "&";
                }
              }
              if (result !== "") {
                result = result.slice(0, result.length - 1);
              }
              config.data = result;
            } else {

              if (opt.multiPart) {
                config.transformRequest = angular.identity;
                config.data = opt.data;
              } else {
                if (!opt.forceQueryString)
                  config.data = opt.data;
                //note config.params will attach it to the query.
              }
            }
          }
          if (opt.verb == 'GET' || opt.forceQueryString) {
            config.params = opt.data;
          }
          if (opt.transformResponse) {
            config.transformResponse = opt.transformResponse;
          }
          if (opt.timeout) config.timeout = opt.timeout;
          $http(config).success(function(data, status, headers, config) {

            try {
              opt.success(data, "success", data);
            } catch (e) {
              console.error(e);
            }
          }).error(function(data, status, headers, config) {
            try {
              opt.error(data, status, data);
            } catch (e) {
              console.error(e);
            }

          });
        } else {
          throw 'Error: Passed null value for opt to httpService implemtation, opt cannot be null';
        }
      }
    }, upload);
  }])
  .factory('session', ['appDomain', '$timeout', '$q', '$cordovaPushV5', 'nav', '$ionicPlatform', '$rootScope', 'backend', '$cordovaAppAvailability', function(appDomain, $timeout, $q, $cordovaPushV5, nav, $ionicPlatform, $rootScope, backend, $cordovaAppAvailability) {
    function Session() {

      function PushNotificationHandler(session) {
        var MESSAGE_TYPE = '411-message-type',
          CLUB_NOTIFICATION = 'club_notification',
          CLUB_RATING = 'club_rating',
          MESSAGE_NOTIFICATION = 'points_notification',
          self = this;
        self.pendingNotifications = [];
        this.readNotification = function(notification) {
          notification._treated = true;
          session.invokeEvent(appDomain.constants.events.notification, notification);
          self.pendingNotifications.removeItem(notification);
        };
        session.on(appDomain.constants.events.notification, function(notification) {
          console.log('---------------------------push notification--------------------------');
          console.log(notification);
          console.log('--------------------------- end --------------------------');
          if (!notification._treated && notification.additionalData.foreground) {
            self.pendingNotifications.push(notification);
            session.invokeEvent('new-pending-notification', {
              notification: notification,
              all: self.pendingNotifications
            });
            return;
          }
          switch (notification.additionalData[MESSAGE_TYPE]) {
            case CLUB_RATING:
              //do something club like.
              console.log('opening club rating');
              session.setTemp('notification', notification.additionalData);
              nav.go(appDomain.constants.paths.club_rating);
              break;

            case CLUB_NOTIFICATION:
            case MESSAGE_NOTIFICATION:
              console.log('a club notification came in');
              session.setTemp('notification', notification.additionalData);
              nav.go(appDomain.constants.paths.message_board);
              break;
          }
        });
      }
      var pref, pref_name = 'The411Session',
        self = this,
        PUSH_TOKEN = 'push_token';
      self.temp = {};

      function getPref() {
        return $q(function(resolve, reject) {
          function check(x) {
            $timeout(function() {
              if (!window.sharedpreferences)
                check(x + 100);
              else {
                if (!pref)
                  sharedpreferences.getSharedPreferences(pref_name, "MODE_PRIVATE", function() {
                    resolve(pref = true);
                  }, reject);
                else
                  resolve(true);
              }

            }, x);
          }
          check(0);
        })
      }

      function resolvePref(cb, reject) {
        getPref().then(cb, function() {
          reject('could not get shared preferences');
        });
      }
      this.setTemp = function(key, value) {
        this.temp['$temp_' + key] = value;
      };

      this.getGoogleAppsName = function(name) {
        return $ionicPlatform.is('android') ? 'com.google.android.apps.maps' :
          $ionicPlatform.is('ios') ? 'comgooglemaps://' : 'googlemaps';
      };

      this.appInstalled = function(appName, cb) {
        switch (appName) {
          case appDomain.constants.apps.google_maps:
            appName = self.getGoogleAppsName(appName);
            break;
        }
        $cordovaAppAvailability.check(appName)
          .then(function() {
            cb(true);
          }, function() {
            cb(false);
          });
      };
      this.launchGoogleMaps = function(location) {
        var sApp = startApp.set({ /* params */
          "action": "ACTION_VIEW",
          "package": self.getGoogleAppsName(),
          "uri": "geo:" + location.latitude + ',' + location.longitude,
        });
        sApp.start();
      };
      this.getTemp = function(key) {
        var value = this.temp[key = '$temp_' + key];
        delete this.temp[key];
        return value;
      };

      this.isActive = function() {
        return self.fetch(_self.constants.session.keys.LOGON).then(function(result) {
          result = JSON.parse(result);
          backend.credentials.token = result.token;
          backend.credentials.username = result.username;
          self.type = result.type;
          return result;
        });
      };

      this.getPendingNotifications = function() {
        return this.__pushHandler.pendingNotifications;
      };

      this.readPendingNotification = function(notification) {
        this.__pushHandler.readNotification(notification);
      };

      this.enableLocation = function(cb) {
        locationPlugin.getLocation([backend.credentials.username, backend.credentials.token], function(res) {
          self.locationEnabled = true;
          if (cb)
            cb(null, res);
        }, function(rs) {
          var permissions = cordova.plugins.permissions;
          permissions.hasPermission([permissions.ACCESS_COARSE_LOCATION, permissions.ACCESS_FINE_LOCATION], checkPermissionCallback, null);

          function checkPermissionCallback(status) {
            if (!status.hasPermission) {
              var errorCallback = function() {
                console.warn('Location permission is not turned on');
                if (cb)
                  cb('User did not grant permissions');
              }

              permissions.requestPermissions(
                [permissions.ACCESS_COARSE_LOCATION, permissions.ACCESS_FINE_LOCATION],
                function(status) {
                  if (!status.hasPermission) {
                    errorCallback();
                    return;
                  }

                  self.invokeEvent('permissionsGranted', {
                    permissions: [appDomain.constants.permissions.location]
                  });
                  if (cb) {
                    cb(null, true);
                  }

                },
                errorCallback);
            }
          }
        });
      }

      this.start = function() {
        self.isActive().then(function() {
          self.enableLocation();
          self.fetch(PUSH_TOKEN).then(function(token) {
            backend.uploadRegistrationToken(token).then(function(res) {
              console.log('registration token uploaded successfully');
            })
          })
        }, function(reason) {
          diag.show({
            title: 'Oops',
            content: 'Something seems to be wrong with your session.'
          });
        })

      }
      this.save = function(key, value) {
        return $q(function(resolve, reject) {
          resolvePref(function() {
            sharedpreferences.putString(key, value, resolve, reject);
          }, reject);
        })
      };

      this.remove = function(key) {
        return $q(function(resolve, reject) {
          resolvePref(function() {
            sharedpreferences.remove(key, resolve, reject);
          }, reject);
        })
      };

      this.fetch = function(key) {
        return $q(function(resolve, reject) {
          resolvePref(function() {
            sharedpreferences.getString(key, resolve, reject);
          }, reject)
        });
      }

      this.logout = function() {
        return $q(function(resolve, reject) {
          self.__pushHandler.pendingNotifications.length = 0;
          resolvePref(function() {
            sharedpreferences.clear(resolve);
            if (self.type == appDomain.constants.LOGON_TYPES.GOOGLE_NATIVE)
              window.plugins.googleplus.logout(
                function(msg) {
                  console.log('successfully logged out of googleplus' + msg);
                },
                function(msg) {
                  console.log('failed to logout google plus native ' + msg);
                }
              );
            locationPlugin.stopService(function() {
              console.log('service stopped successfully');
            }, function(e) {
              console.log('couldnt stop the gps service...')
            });
          }, reject);
        })
      }

      this.isActive();
      this.on('permissionsGranted', function(event) {
        if (event.permissions.indexOf(appDomain.constants.permissions.location) == -1)
          self.enableLocation();
      });
      // initialize plugins
      $ionicPlatform.ready(function() {
        $cordovaPushV5.initialize({
          android: {
            senderID: "966481672980"
          }
        }).then(function() {
          // start listening for new notifications
          $cordovaPushV5.onNotification();
          // start listening for errors
          $cordovaPushV5.onError();

          // register to get registrationId
          $cordovaPushV5.register().then(function(registrationId) {
            self.save(PUSH_TOKEN, registrationId).then(function() {
              console.log('successfully stored push notitication token:' + registrationId);
            })
          })
        });

        // triggered every time notification received
        $rootScope.$on('$cordovaPushV5:notificationReceived', function(event, data) {
          // console.log('a push notification came through...');
          $timeout(function() {
            self.invokeEvent(appDomain.constants.events.notification, data);
          }, 0)
        });

        // triggered every time error occurs
        $rootScope.$on('$cordovaPushV5:errorOcurred', function(event, e) {
          // e.message
          console.log(e);
        });
      });

      this.__pushHandler = new PushNotificationHandler(this);
    }
    appDomain.inherit(appDomain.EventObject, Session);


    return new Session();
  }])
  .filter('from', function() {
    return function(input) {
      return moment(input).fromNow() || 'Who knows ?';
    };
  })
  .filter('map', function() {
    function effectiveDeviceWidth() {
      var deviceWidth =
        window.orientation === 0 ? window.innerWidth : window.innerHeight;
      // iOS returns available pixels, Android returns pixels / pixel ratio
      // http://www.quirksmode.org/blog/archives/2012/07/more_about_devi.html
      // if (navigator.userAgent.indexOf('Android') >= 0 && window.devicePixelRatio) {
      //   deviceWidth = deviceWidth / window.devicePixelRatio;
      // }
      return Math.floor(deviceWidth);
    }
    return function(input) {
      console.log(arguments);
      console.log('map filter called:' + input);
      return input + '&size=' + (effectiveDeviceWidth()) + 'x600&scale=' + ((window.devicePixelRatio > 1) ? 1 : 1);
    };
  });