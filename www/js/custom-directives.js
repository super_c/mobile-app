(function() {

	var mod = angular.module('411-directives', []);
	mod.directive('fourInput', function($timeout) {
			/**
			 * Add querySelectorAll() to jqLite.
			 *
			 * jqLite find() is limited to lookups by tag name.
			 * TODO This will change with future versions of AngularJS, to be removed when this happens
			 *
			 * See jqLite.find - why not use querySelectorAll? https://github.com/angular/angular.js/issues/3586
			 * See feat(jqLite): use querySelectorAll instead of getElementsByTagName in jqLite.find https://github.com/angular/angular.js/pull/3598
			 */
			if (angular.element.prototype.querySelectorAll === undefined) {
				angular.element.prototype.querySelectorAll = function(selector) {
					return angular.element(this[0].querySelectorAll(selector));
				};
			}
			var template = '<label class="input-container">' +
				'<span class="four-input-label input-label-focus"></span>' +
				'<input class="input-content" >' +
				'</label>';
			return {
				requires: '?ngModel',
				replace: true,
				restrict: 'E',
				template: '<div></div>',
				compile: function(container, n) {
					var el = angular.element(template);
					container.replaceWith(el);
					container = el;
					var a = container.find("input");
					var l = container[0].querySelector(".four-input-label");
					l.innerHTML = n.placeholder;
					angular.forEach({
						name: n.name,
						"multi-regex":n.multiRegex,
						type: n.type,
						"ng-value": n.ngValue,
						"ng-model": n.ngModel,
						required: n.required,
						"ng-required": n.ngRequired,
						"ng-minlength": n.ngMinlength,
						"ng-maxlength": n.ngMaxlength,
						"ng-pattern": n.ngPattern,
						"ng-change": n.ngChange,
						"ng-trim": n.trim,
						"ng-blur": n.ngBlur,
						"ng-focus": n.ngFocus
					}, function(e, n) {
						angular.isDefined(e) && a.attr(n, e)
						el.removeAttr(n);
					});
					return function($scope, elem, attr) {

						var input = angular.element(elem[0].querySelector('.input-content'))
							// input.addClass(attr.highlight);
						input.addClass(attr.highlight + '-border');

					}
				}
			}
		}).directive('backImg', function() {
			return function(scope, element, attrs) {
				attrs.$observe('backImg', function(value) {
					element.css({
						'background-image': 'url(' + value + ')',
						'background-size': 'cover'
					});
				});
			};
		}).directive('ngEnter', function() {
			return function(scope, element, attrs) {
				element.bind("keydown keypress", function(event) {
					if (event.which === 13) {
						scope.$apply(function() {
							scope.$eval(attrs.ngEnter);
						});

						event.preventDefault();
					}
				});
			};
		})
		.directive('multiRegex', function() {
			return {

				// limit usage to argument only
				restrict: 'A',

				// require NgModelController, i.e. require a controller of ngModel directive
				require: 'ngModel',

				// create linking function and pass in our NgModelController as a 4th argument
				link: function(scope, element, attrs, ctrl) {
					var regexes;
					// please note you can name your function & argument anything you like
					function customValidator(ngModelValue) {

						if (regexes && regexes.length > 0 && regexes instanceof Array) {
							for (var i = 0; i < regexes.length; i++) {
								if (new RegExp(regexes[i].regex).test(ngModelValue)) {
									ctrl.$setValidity(regexes[i].name, true);
								} else {
									ctrl.$setValidity(regexes[i].name, false);
								}
							}
						}
						return ngModelValue;
					}
					var ticket = scope.$watch(attrs.multiRegex, function(value) {
						regexes = value;
					})
					scope.$on('$destroy', function() {
						ticket();
					});
					// we need to add our customValidator function to an array of other(build-in or custom) functions
					// I have not notice any performance issues, but it would be worth investigating how much
					// effect does this have on the performance of the app
					ctrl.$parsers.push(customValidator);
				}
			};
		});

}())