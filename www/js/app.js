// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'ionic-material', 'controllers', '411-directives', 'ion-floating-menu', 'services', 'ngCordova', 'ngRating', 'ionMdInput', 'ionic-native-transitions','ngFileUpload'])
  .run(function($ionicPlatform, $ionicNativeTransitions, $rootScope) {
    $rootScope.emailPattern = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/;
    $rootScope.$on('$stateChangeSuccess', function(evt, toState) {
      console.log('stateChangeSuccess');
      if (toState.lightBar) {
        $rootScope.lightBar = true;
      } else {
        $rootScope.lightBar = false;
      }
    });
    //$ionicNativeTransitions.enable(true);
    $ionicPlatform.ready(function() {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

        // Don't remove this line unless you know what you are doing. It stops the viewport
        // from snapping when text inputs are focused. Ionic handles this internally for
        // a much nicer keyboard experience. 
        //cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
    });

  })
  .config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $ionicNativeTransitionsProvider) {
    $ionicConfigProvider.views.transition('none');
    $ionicConfigProvider.scrolling.jsScrolling(false);
    // $ionicNativeTransitionsProvider.setDefaultOptions({
    //   // type: 'slide',
    //   // direction: 'right',
    //   //"duration": 300,
    //   // "androiddelay": 400,
    //   // slowdownfactor: 4,
    //   // backInOppositeDirection: true
    // });
    $ionicNativeTransitionsProvider.setDefaultTransition({
      type: 'fade',
      duration:500,
      // type:'slide',
      // direction: 'left'
    });
    $ionicNativeTransitionsProvider.setDefaultBackTransition({
      type: 'fade',
      duration:100
      // type:'slide',
      // // duration: 200,
      // direction: 'right'
    });
    $stateProvider
      .state('login', {
        url: '/login',
        nativeTransitions: null,
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl',
        cache: false
      })
      .state('registration', {
        url: '/registration',
        templateUrl: 'templates/registration.html',
        controller: 'RegCtrl'
      })
      .state('forgot-password', {
        url: '/forgot-password',
        templateUrl: 'templates/forgot-password.html',
        controller: 'ForgotPasswordCtrl'
      })
      .state('home', {
        url: '/home',
        templateUrl: 'templates/home.html',
        controller: 'HomeCtrl',
        cache: false
      })
      .state('home.club_rating', {
        url: '/club-rating',
        views: {
          'menuContent': {
            templateUrl: 'templates/club-rating.html',
            controller: 'ClubRatingCtrl'
          }
        }
      })
        .state('home.new_place', {
        url: '/new-place',
        views: {
          'menuContent': {
            templateUrl: 'templates/new-place.html',
            controller: 'NewPlaceCtrl'
          }
        }
      })

       .state('home.change-password', {
        url: '/change-password',
        views: {
          'menuContent': {
            templateUrl: 'templates/change-password.html',
            controller: 'ChangePasswordCtrl'
          }
        }
      })
      .state('home.account', {
        url: '/account/:id',
        params: {
          id: {
            value: null,
            squash: true
          }
        },
        views: {
          'menuContent': {
            templateUrl: 'templates/profile.html',
            controller: 'ProfileCtrl'
          }
        }
      })
      .state('home.place', {
        url: '/place-detail',
        // abstract:true,
        views: {
          'menuContent': {
            templateUrl: 'templates/place-detail.html',
            controller: 'PlaceDetailCtrl'
          }
        }
      })
      .state('home.search-for-place', {

        url: '/search-for-place',
        views: {
          'menuContent': {
            templateUrl: 'templates/search-for-place.html',
            controller: 'SearchForPlaceCtrl'
          }
        },
        lightBar: true
      })
      .state('home.notification-center', {

        url: '/notification-center',
        views: {
          'menuContent': {
            templateUrl: 'templates/notification-center.html',
            controller: 'NotificationCenterCtrl'
          }
        }
      })

    .state('home.dashboard', {
        url: '/dashboard',
        views: {
          'menuContent': {
            templateUrl: 'templates/dashboard.html',
            controller: 'DashboardCtrl'
          }
        }
      })
      .state('home.message_board', {
        url: '/message-board',
        views: {
          'menuContent': {
            templateUrl: 'templates/message-board.html',
            controller: 'MessageBoardCtrl'
          }
        }
      })
    var info = {
      name: 'place-info',
      url: '/place-info',
      parent: 'home.place',
         nativeTransitions: {
        type: 'fade',
        androiddelay: 100,
        duration: 10
      },
      views: {
        'content': {
          templateUrl: 'templates/place-info.html'
        }
      }
    };
    var comments = {
      name: 'place-comments',
      url: '/place-comments', 
      parent: 'home.place',
      nativeTransitions: {
        type: 'fade',
        androiddelay: 100,
        duration: 10
      },
      views: {
        'content': {
          templateUrl: 'templates/place-comments.html'
        }
      }
    };

    $stateProvider.state(info);
    $stateProvider.state(comments);

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');
  });