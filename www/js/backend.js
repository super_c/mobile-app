function com_scheme_labs_backend(_self) {

    function Backend(httpService, uploadService) {
        var _backRef = this;
        this.HTTPVERBS = {
            POST: 'POST',
            GET: 'GET',
            DELETE: 'DELETE',
            PUT: 'PUT'
        };
        this.RETURNTYPES = {
            JSON: 'JSON',
            XML: 'XML',
            HTML: 'HTML',
            STRING: 'STRING'
        };
        this.getGoogleAuthPath = function() {
            return this.credentials.apiUrl + this.credentials.googleAuthPath;
        };
        this.getGoogleDonePath = function() {
            return this.credentials.gooogleDonePath;
        };
        this.getGoogleFailPath = function() {
            return this.credentials.googleFailedPath;
        };
        this.credentials = {
            appKey: '984b36d97bf84e6b589933e6fb171355289d0ad729c70cf33b7f8902786ef4db',
            appSecret: 'a353d08b96355948c61954eec9a23bbda5b28edcd2f97276c25c48c8c21f661c',
            apiUrl: 'http://10.0.2.2:4646/',
            googleAuthPath: '_auth/api/user/auth/google',
            gooogleDonePath: '_auth/api/user/auth/google/success',
            googleFailedPath: '_auth/api/user/auth/google/failed',
            username: ''
        };

        function Command(verb, method, requiresAuth, data, returnType) {
            this.verb = verb;
            this.method = method;
            this.requiresAuth = requiresAuth;
            this.data = data;
            this.apiUrl = _backRef.credentials.apiUrl;
            this.returnType = !returnType ? _backRef.RETURNTYPES.JSON : returnType;
            this.baseList = ['username', 'baseUrl', 'timestamp', 'applicationKey', 'params', 'token', 'nonce'];
        }

        function AuthCommand(verb, method, requiresAuth, data, returnType) {
            Command.apply(this, Array.apply(null, arguments));
            this.getUrl = function() {
                return this.apiUrl + '_auth/api/' + this.method;
            };
        }

        function GeoCommand(verb, method, requiresAuth, data, returnType) {
            Command.apply(this, Array.apply(null, arguments));
            this.getUrl = function() {
                return this.apiUrl + '_geo/' + this.method;
            };
        }

        function PushCommand(verb, method, requiresAuth, data, returnType) {
            Command.apply(this, Array.apply(null, arguments));
            this.getUrl = function() {
                return this.apiUrl + '_push/' + this.method;
            };
        }
        Command.prototype.getNonce = function() {
            return new Date().getTime();
        };
        Command.prototype.getTimestamp = function() {
            return moment.utc().valueOf();
        };
        Command.prototype.execute = function(parser, errParser) {
            var self = this;
            return new _self.Promise(function(resolve, reject) {

                //make the request.
                try {
                    if (!self.apiUrl) {
                        window.console.log('API URL IS ABSENT');
                        return;
                    }
                    self.fullUrl = self.getUrl ? self.getUrl(self.apiUrl || _backRef.credentials.apiUrl) : (self.apiUrl || _backRef.credentials.apiUrl);
                    //authenticate if required
                    if (self.requiresAuth) {
                        self.authenticate();
                    }
                    if (!self.skipContentType && self.verb == _backRef.HTTPVERBS.POST)
                        self.headers ? (self.headers["Content-Type"] ? '' : self.headers["Content-Type"] = "application/json") : self.headers = {
                            "Content-Type": "application/json"
                        };

                    httpService.request({
                        url: self.fullUrl,
                        verb: self.verb,
                        data: self.data,
                        headers: self.headers,
                        timeout: self.timeout,
                        forceQueryString: self.forceQueryString, // this is possible to allow for quirks in API's
                        success: function(res) {
                            switch (self.returnType) {
                                case _backRef.RETURNTYPES.XML:
                                    res = xmlParser.parse(res);
                                    break;
                                case _backRef.RETURNTYPES.HTML:
                                    res = domParser.parse(res);
                                    break;
                                case _backRef.RETURNTYPES.JSON:
                                    res = typeof res == 'string' ? JSON.parse(res) : res;
                                    break;
                            }

                            var result = parser ? parser(res.response || res) : res.response || res;
                            if (result && typeof _self.constants.status.sessionExpired !== 'undefined' && result.status == _self.constants.status.sessionExpired) {
                                _backRef.invokeEvent('session-expired', result);
                                return;
                            }

                            resolve(result);

                        },
                        error: function(reason) {

                            reject(errParser ? errParser(reason) : reason);
                        },
                        transformResponse: self.customResponseTransform
                    });
                } catch (e) {
                    console.log(e);
                    reject(e);
                }


            });
        }
        Command.prototype.authenticate = function() {
            var username = _backRef.credentials.username || this.data.username,
                signatureBaseString = '',
                appKey = _backRef.credentials.appKey,
                baseUrl = this.fullUrl,
                nonce = this.getNonce(),
                timestamp = this.getTimestamp(),
                parameters = [];
            var header = 'username=' + encodeURIComponent(username) + ';applicationKey=' + encodeURIComponent(appKey) + ';baseUrl=' + encodeURIComponent(baseUrl) + ';nonce=' + encodeURIComponent(nonce) + ';timestamp=' + encodeURIComponent(timestamp) + ';cipher=HMACSHA1';
            var baseList = this.baseList;
            baseList.sort();
            for (var o = 0; o < baseList.length; o++) {
                switch (baseList[o]) {
                    case 'username':
                        signatureBaseString += username;
                        break;
                    case 'timestamp':
                        signatureBaseString += timestamp;
                        break;
                    case 'baseUrl':
                        signatureBaseString += baseUrl;
                        break;
                    case 'nonce':
                        signatureBaseString += nonce;
                        break;
                    case 'token':
                        signatureBaseString += _backRef.credentials.token;
                        break;
                    case 'applicationKey':
                        signatureBaseString += appKey;
                        break;
                    case 'params':
                        header += ';params:';
                        paramsHeader = '';
                        params = [];
                        for (var prop in this.data) {
                            if (this.data.hasOwnProperty(prop)) {
                                params.push(prop);
                                paramsHeader += prop + '=' + encodeURIComponent(this.data[prop]);
                                paramsHeader += ',';
                            }
                        }
                        params.length > 0 ? paramsHeader = paramsHeader.replace(/\,$/, '') : '';
                        header += encodeURIComponent(paramsHeader);
                        params.sort();
                        for (var i = 0; i < params.length; i++) {
                            signatureBaseString = signatureBaseString.concat(params[i], this.data[params[i]]);
                        }
                        break;
                }
            }

            var secretBytes = CryptoJS.enc.Utf8.parse(_backRef.credentials.appSecret);
            var sigBytes = CryptoJS.enc.Utf8.parse(signatureBaseString);
            console.log('signature base string:' + signatureBaseString);
            var signature = (CryptoJS.HmacSHA1(sigBytes, secretBytes)).toString(CryptoJS.enc.Base64);
            header += ';signature=' + encodeURIComponent(signature);
            this.headers ? this.headers.Authorization = header : this.headers = {
                Authorization: header
            };
        };
        _self.inherit(Command, AuthCommand);
        _self.inherit(Command, GeoCommand);
        _self.inherit(Command, PushCommand);

        function noSessionBase() {
            return ['username', 'baseUrl', 'timestamp', 'applicationKey', 'params', 'nonce'];
        }

        function machineBase() {
            return ['baseUrl', 'timestamp', 'applicationKey', 'params', 'nonce'];
        }
        //=== service methods ===//
        this['reverseGeocode'] = function(lon, lat) {
            var command = new Command(this.HTTPVERBS.GET, '', false, {
                lon: lon,
                lat: lat,
                format: 'json'
            });
            command.apiUrl = "http://nominatim.openstreetmap.org/reverse";
            return command.execute();
        }
        this['login'] = function(user) {
            var command = new AuthCommand(this.HTTPVERBS.POST, 'user/login', true, {
                username: user.username,
                password: user.password
            });
            command.timeout = 60000;
            command.baseList = noSessionBase();
            return command.execute();
        };
        this['verifyGoogle'] = function(token) {
            var command = new AuthCommand(this.HTTPVERBS.POST, 'user/auth/google/verify', true, {
                token: token
            });
            command.timeout = 60000;
            command.baseList = machineBase();
            return command.execute();
        };
        this['lookup'] = function(key) {
            var command = new AuthCommand(this.HTTPVERBS.GET, 'user/lookup', true, {
                key: key
            });
            command.baseList = noSessionBase();
            return command.execute();
        };
        this['forgotPassword'] = function(username) {
            var command = new AuthCommand(this.HTTPVERBS.POST, 'user/forgot-password', true, {
                username: username
            });
            command.baseList = noSessionBase();
            return command.execute();
        };
        this['loginWithGoogle'] = function(username, token) {
            var command = new AuthCommand(this.HTTPVERBS.POST, 'user/auth/google/login', true, {
                username: username,
                oauth_token: token,
            });
            command.baseList = noSessionBase();
            return command.execute();
        };
        this['register'] = function(user) {
            var command = new AuthCommand(this.HTTPVERBS.POST, 'user/register', true, user);
            command.baseList = noSessionBase();
            return command.execute();
        };
        this['changePassword'] = function(current, password) {
            return new AuthCommand(this.HTTPVERBS.POST, 'user/change-password', true, {
                newPassword: password,
                currentPassword: current
            }).execute();

        };
        this['userExists'] = function(username) {
            var command = new AuthCommand(this.HTTPVERBS.GET, 'user/exists', true, {
                username: username
            });
            command.baseList = noSessionBase();
            return command.execute();
        };
        this['logout'] = function() {
            return new AuthCommand(this.HTTPVERBS.POST, 'user/logout', true, {
                username: _backRef.credentials.username
            }).execute();
        };
        this['uploadRegistrationToken'] = function(token) {
            return new PushCommand(this.HTTPVERBS.POST, 'register', true, {
                username: _backRef.credentials.username,
                token: token
            }).execute();
        };
        this['rateClub'] = function(rate, location, message) {
            return new GeoCommand(this.HTTPVERBS.POST, 'update-query', true, {
                rating: rate,
                location_name: location,
                comment: message,
                type: 'club_rating',
                username: _backRef.credentials.username
            }).execute();
        };
        this['getPlaces'] = function() {
            return new GeoCommand(this.HTTPVERBS.GET, 'places', true, {
                username: _backRef.credentials.username
            }).execute();
        };
        this['createNewPlace'] = function(data, screenshot) {
            var command = new GeoCommand(this.HTTPVERBS.POST, 'places/new-place', true, data);
            command.authenticate();

            return new _self.Promise(function(resolve, reject) {
                function rn() {
                    uploadService.upload({
                        url: _backRef.credentials.apiUrl + '_geo/places/new-place',
                        headers: command.headers,
                        data: data
                    }).then(function(d) {
                        resolve(d.data);
                    }, reject);
                }
                if (uploadService.isFile(screenshot)) {
                    data.screenshot = screenshot;
                    rn();
                    return;
                }

                uploadService.urlToBlob(screenshot).then(function(blob) {
                    data.screenshot = blob;
                    rn();
                });

            });

        };
        this['getPlace'] = function(id) {
            return new GeoCommand(this.HTTPVERBS.GET, 'places/{id}'.replace('{id}', id), true).execute();
        };
        this['getUserInfo'] = function(id) {
            return new GeoCommand(this.HTTPVERBS.GET, 'user/info', true, {
                query: id
            }).execute();
        };
        this['searchForPlace'] = function(query) {
            return new GeoCommand(this.HTTPVERBS.GET, 'place/search/{query}'.replace('{query}', query), true).execute();
        };
    }
    //setup inheritance.
    _self.inherit(_self.EventObject, Backend);
    return Backend;
}