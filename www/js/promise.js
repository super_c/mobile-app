function com_scheme_labs_promise() {
    return (function promise(defered) {
        var self = this;
        var r = null;
        var e = null;
        var d = null;
        var PENDING = 'PENDING';
        var REJECTED = 'REJECTED';
        var FULFILLED = 'FULFILLED';
        var state = PENDING;
        var result = null;

        function series(arr) {
            return function(result) {
                for (var i = 0; i < arr.length; i++)
                    try {
                        if (arr[i] && typeof(arr[i]) == 'function')
                            arr[i](result);

                        if (arr[i] && arr[i] instanceof Array)
                            arr[i][1](result);
                    } catch (e) {}
            };
        }
        this.then = function(fn, err) {
            return new promise(function(resolve, reject) {

                r = series([fn, resolve]);
                e = series([err, reject]);
                if (state == FULFILLED && fn)
                    r(result);

                if (state == REJECTED && err)
                    e(result);

            });
        };
        this.done = function(_done) {
            return self.then(_done, _done);
        };

        defered(function(res) {
            state = FULFILLED;
            if (r) {
                r(res);

            } else {
                result = res;
            }
        }, function(reason) {
            state = REJECTED;
            if (e) {
                e(reason);

            } else {
                result = reason;
            }
        });
    });
}