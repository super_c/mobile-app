function com_scheme_labs_viewmodels(_self) {

	var viewmodels = {
		Base: function(backend, loading, diag) {
			var self = this;
			this.busy = false;
			this.tickets = [];
			this.addEventHandler = function(eventobject, eventName, handler) {
				if (typeof eventName == 'string') {
					var detach = eventobject.on(eventName, handler);
					this.tickets.push(detach);
				}
				if (eventName instanceof Array) {
					eventName.forEach(function(item) {
						self.addEventHandler(eventobject, item, handler);
					});
				}

			};
			this.destroy = function() {
				try {
					destroyed++; //global counter;
					this.destroyed = true;
					this.tickets.forEach(function(detach) {
						detach();
					});
				} catch (e) {

				}
			};
			this.invoke = function(name, success, error) {

				if (!loading.isBusy() || this.parrallelInvocationAllowed) {
					this.busy = true;
					this.summary = null;
					loading.show('please wait...');
					var args = (arguments.length === 3 ? [] : Array.apply(null, arguments)).slice(3);
					return backend[name].apply(backend, args).then(success, error || function(response) {
						diag.show({
							title: 'Error',
							content: response ? response.responseMessage : 'No response for our server, something might be wrong , please retry at another time.'
						});
					}).done(function() {
						loading.hide();
						self.busy = false;
					});
				}
			};
		},
		Login: function(backend, diag, nav, loading, browser, session) {
			var self = this,
				USERNAME = 'USERNAME',
				AUTH_TOKEN = 'AUTH_TOKEN';
			_self.viewmodels.Base.call(this, backend, loading, diag);
			this.username = '';
			this.password = '';
			this.hasVerifiedSession = false;

			function setup(responseObject, type) {
				backend.credentials.token = responseObject.token;
				backend.credentials.username = responseObject.user.username;

				session.save(AUTH_TOKEN, backend.credentials.token).then(function() {
					session.save(USERNAME, backend.credentials.username).then(function() {
						session.save(_self.constants.session.keys.LOGON, JSON.stringify({
							username: backend.credentials.username,
							type: type,
							token: backend.credentials.token
						})).then(function() {
							session.start();
						});
					});
				});
				nav.go(_self.constants.paths.dashboard, true);

			}
			this.doLogin = function() {
				this.invoke('login', function(res) {
					setup(res.responseObject, _self.constants.LOGON_TYPES.NATIVE);
				}, self.parseErrors, {
					username: self.username,
					password: self.password
				});
			};

			function googleWeb() {
				var opts = {
					url: backend.getGoogleAuthPath(),
					handlers: {
						loadstop: function(event) {
							console.log('load end event:');
							console.log(event);
							if (event.url.indexOf(backend.getGoogleDonePath()) !== -1) {

								browser.executeScript({
									code: 'data()'
								}, function(values) {
									//data is a function in the page that returns the oauth_token
									//means it successfully created the token from google.
									//extract the auth_token from the redirect url and close broweser.
									//window.alert(values);
									browser.close();
									self.invoke('loginWithGoogle', function(res) {
										setup(res.responseObject, _self.constants.LOGON_TYPES.GOOGLE);
									}, self.parseErrors, _self.getQueryStringValue(event.url, 'username'), values[0]);
								});
							}

							if (event.url.indexOf(backend.getGoogleFailPath()) !== -1) {
								browser.close();
								diag.show({
									title: 'Failed',
									content: 'Oops something went wrong, please retry'
								});
							}
						},
						exit: function(event) {
							console.log('exit event:');
							console.log(event);
						}
					},
					callback: function() {
						console.log('cordova in appbrowser has called the callback.');
					}
				};
				browser.go(opts);
			}

			function googleNative() {
				var that = this;
				window.plugins.googleplus.login({
						webClientId: '966481672980-mt4t3r1sb7drp74fa4a2tos27l3n0u0r.apps.googleusercontent.com'
					},
					function(user_data) {
						// For the purpose of this example I will store user data on local storage
						var successful = false;
						that.invoke('verifyGoogle', function(serverResult) {
							successful = true;
						}, null, user_data.idToken).then(function() {
							that.invoke('loginWithGoogle', function(res) {
								setup(res.responseObject, _self.constants.LOGON_TYPES.GOOGLE_NATIVE);
							}, that.parseErrors, user_data.email, user_data.idToken);
						});
					},
					function(msg) {
						diag.show({
							title: 'Error',
							content: JSON.stringify(msg)
						});
					}
				);
			}
			this.doGoogleLogin = function() {
				var that = this;
				window.plugins.googleplus.isAvailable(function(avail) {
					if (avail) {
						googleNative.call(that);
						return;
					}

					googleWeb.call(that);
				});

			};
			this.init = function() {
				session.isActive().then(function(logon) {
					if (logon.username && logon.token) {
						setup({
							user: {
								username: logon.username
							},
							token: logon.token
						});
					}
					return logon;

				}, function() {
					self.hasVerifiedSession = true;
				});
			};
		},
		ChangePassword: function(backend, diag, nav, loading) {
			var self = this;
			_self.viewmodels.Base.call(this, backend, loading, diag);
			this.init = function() {
				self.invoke('lookup', function(result) {

					if (result && result.responseObject) {
						self.passwordPatterns = JSON.parse(result.responseObject.value);
					}

				}, self.getErrors, _self.constants.lookups.PASSWORD_POLICY);
			};
			this.changePassword = function() {
				self.invoke('changePassword', function() {
					nav.go(_self.constants.paths.dashboard)
				}, null, this.currentPassword, this.newPassword);
			};
		},
		Registration: function(backend, nav, loading, diag) {
			var self = this;
			_self.viewmodels.Base.call(this, backend, loading, diag);
			this.init = function() {
				self.invoke('lookup', function(result) {

					if (result && result.responseObject) {
						self.passwordPatterns = JSON.parse(result.responseObject.value);
					}

				}, self.getErrors, _self.constants.lookups.PASSWORD_POLICY);
			};
			this.register = function() {
				if (!self.usernameError)
					this.invoke('register', function(result) {
						diag.show({
							title: 'Successfully registered',
							content: 'An email has been sent to your you, please verify your account by clicking the link in the email. Thanks for joining thefourone-one. :)'
						});
						nav.go(_self.constants.paths.login, true);
					}, self.getErrors, {
						username: self.username,
						display_name: self.display_name,
						password: self.password
					});
			};
			this.checkIfUserExists = function() {
				if (self.username)
					this.invoke('userExists', function(result) {
						self.usernameError = result.responseObject.exists ? 'Username already exists' : null;
					}, function() {
						self.usernameError = null;
					}, self.username);
			};
		},
		ForgotPassword: function(backend, nav, loading, diag) {
			var self = this;
			_self.viewmodels.Base.call(this, backend, loading, diag);
			this.forgotPassword = function() {
				self.invoke('forgotPassword', function(res) {
					diag.show({
						title: 'Success',
						content: 'An email has been sent to your address'
					});
					nav.back();
				}, null, self.username);
			};
		},
		Home: function(backend, session, diag, loading) {
			_self.viewmodels.Base.call(this, backend, loading, diag);
			this.init = function() {
				if (session.locationEnabled) {
					session.enableLocation();
				}
			};
		},
		MessageBoard: function(session) {
			_self.viewmodels.Base.call(this);
			var self = this;
			this.init = function() {
				var message = session.getTemp('notification');
				this.message = {
					full_message: message[_self.constants.notifications.full_message],
					location: message[_self.constants.notifications.location],
					pic: message[_self.constants.notifications.pic]
				};
			};

		},
		SearchForPlace: function(backend, session, diag, loading, nav) {
			_self.viewmodels.Base.call(this, backend, loading, diag);
			var self = this;
			this.search = function() {
				if (self.query) {
					self.invoke('searchForPlace', function(result) {
						self.places = result.responseObject;
					}, null, self.query);
				}
			};
			this.goToPlace = function(place) {
				session.setTemp('place', place);
				nav.go(_self.constants.paths.place);
			};
		},
		ClubRating: function(backend, session, diag, loading, nav) {
			_self.viewmodels.Base.call(this, backend, loading, diag);
			var self = this;
			this.rating = 0;
			this.init = function() {
				var message = session.getTemp('notification');
				var location = message[_self.constants.notifications.location],
					locations = location.split('|'),
					l = '';
				if (locations.length == 1) {
					try {
						l = location.split(':')[0];
					} catch (e) {

					}
				} else {
					var temp = [];
					locations.forEach(function(x) {
						var name_display = x.split(':');
						temp.push({
							name: name_display[0],
							display_name: name_display[1]
						});
					});
					locations = temp;
				}
				this.message = {
					full_message: message[_self.constants.notifications.full_message],
					location: l,
					locations: locations
				};
			};
			this.setRate = function() {
				this.invoke('rateClub', function(e) {
					console.log('successfully rated club..');
					nav.back();
				}, null, this.rating, this.message.location, this.comments);
			};
		},
		Dashboard: function(backend, loading, diag, nav, session, timer) {
			_self.viewmodels.Base.call(this, backend, loading, diag);
			var self = this;
			var actions = [{
				button: {
					text: 'Update your current Location'
				},
				action: function() {
					self.getLocation();
				}
			}, {
				button: {
					text: '<b> Logout </b>'
				},
				action: function() {
					self.logout();
				}
			}];
			if (session.type == _self.constants.LOGON_TYPES.NATIVE)
				actions.splice(1, 0, {
					button: {
						text: 'Change Password'
					},
					action: function() {
						nav.go(_self.constants.paths.change_password);
					}
				});
			this.actionClicked = function(index) {
				actions[index].action();
			};
			this.getActionButtons = function() {
				var buttons = [];
				actions.forEach(function(item) {
					buttons.push(item.button);
				});
				return buttons;
			};

			function refreshLoop() {
				var attempts = 0,

					loop = {
						start: function() {
							self.getLocation(function(er) {
								if (er)
									attempts++;
								else {
									timer.get()(self.reload, 3500);
									return;
								}

								if (attempts >= 3) {
									diag.show({
										title: 'Error',
										content: 'Could not retrieve your location..'
									});
									return;
								}
								loop.start();
							});
						}
					};
				return loop;
			}
			this.init = function() {
				this.reload();
				this.addEventHandler(session, ['permissionsGranted', 'placeRated'], function() {
					refreshLoop().start();
				});
			};
			this.pendingNotifications = function() {
				return session.getPendingNotifications().length;
			};
			this.logout = function() {
				diag.confirm({
					content: 'Are you sure you want to logout ?',
					title: 'Logout',
					noText: 'no',
					yes: function() {
						session.logout().then(function() {
							console.log('about to fetch username');
							session.fetch('USERNAME').then(function(n) {
								console.log('name:' + n);
							}, function(er) {
								console.log('er:' + er);
							});
						});
						self.invoke('logout', function(res) {
							nav.go(_self.constants.paths.login, true);
						}, function(er) {
							console.log('an error occurred during logout , but still navigate to home screen');
							nav.go(_self.constants.paths.login, true);
						});
					}
				});

			};
			this.pull = function() {
				backend.getPlaces().then(function(res) {
					self.places = res.responseObject;
				}).done(function() {
					self.invokeEvent('loaded');
				});
			};
			this.reload = function() {
				this.invoke('getPlaces', function(r) {
					self.places = r.responseObject;
					self.invokeEvent('loaded');
				});
			};
			this.goToPlace = function(place) {
				session.setTemp('place', place);
				nav.go(_self.constants.paths.place);
			};

			this.goToUploadNewPlace = function() {
				nav.go(_self.constants.paths.new_place);
			};

			this.getLocation = function(cb) {
				session.enableLocation(function(er, res) {
					if (!er && res) {
						diag.toast('Location updated', diag.SHORT_DURATION, diag.BOTTOM_POSITION);

					}
					if (cb) cb(er);
				});
			};
		},
		PlaceDetail: function(backend, diag, loading, session, nav, timer, socialSharing) {
			_self.viewmodels.Base.call(this, backend, loading, diag);
			var self = this;
			this.WHATSAPP = 'whatsapp';
			this.TWITTER = 'twitter';
			this.FACEBOOK = 'facebook';
			this.OTHER = 'other';

			function maybe() {
				var model = {
					rating: 0,
					title: 'What\'s thefourone-one ?',
					subText: 'Hey are you in ' + self.place.display_name,
					setRate: function() {
						self.invoke('rateClub', function(e) {
							console.log('successfully rated club..');
							model.close();
							timer.get()(function() {
								self.pull();
								session.invokeEvent('placeRated', self.place);
							}, 1500);

						}, null, model.rating, self.place.name, model.comments);
					}
				};
				diag.show({
					fromTemplate: _self.constants.templates.user_maybe_in_location,
					content: model
				});
			}
			this.refresh = function() {
				this.invoke('getPlace', function(res) {
					self.place = res.responseObject;
					if (self.place.user_maybe_in_location) {
						maybe();
					}
					timer.get()(function() {
						nav.goState(_self.constants.states.place.info);
					}, 100);
				}, null, this.place.name);
			};

			this.pulledBack = function() {
				//console.log('great!!!!!!');
			};
			this.selectTab = function(name) {
				nav.goState(name);
			};
			this.goToProfile = function(username) {
				//nav.go(_self.constants.paths.profile + '/' + username, false);
				var model = new _self.viewmodels.Profile(backend, diag, loading, session, nav);
				diag.show({
					fromTemplate: _self.constants.templates.profile,
					content: model
				});
				timer.get()(function() {
					model.init({
						id: username
					});
				}, 0);
			};
			this.openMap = function() {
				session.appInstalled(_self.constants.apps.google_maps, function(installed) {
					if (!installed) {
						console.log('google maps is not installed....');
					}
					session.launchGoogleMaps({
						longitude: self.place.longitude,
						latitude: self.place.latitude
					});
				});
			};
			this.pull = function() {
				backend.getPlace(this.place.name).then(function(res) {
					self.place = res.responseObject;
					if (self.place.user_maybe_in_location) {
						maybe();
					}
				}).done(function() {
					self.invokeEvent('loaded');
				});
			};

			function getSocialMessage(type) {

				var opts = {
					method: 'share',
					params: ['Want to know thefourone-one on ' + self.place.display_name +
						' ?, download the app https://play.google.com/store/apps/details?id=com.scheme.thefouroneone'
					]
				};
				switch (type) {
					case 'twitter':
						opts.method = 'shareViaTwitter';
						break;
					case 'facebook':
						opts.method = 'shareViaFacebook';
						break;
					case 'whatsapp':
						opts.method = 'shareViaWhatsApp';
						break;
				}
				return opts;
			}

			function shared() {
				diag.toast('Successfully shared', diag.SHORT_DURATION, diag.BOTTOM_POSITION);
			}

			function couldntShare(err) {
				diag.show({
					title: 'Problem with sharing',
					content: 'You may not have the selected app installed'
				});
			}
			this.share = function(type) {
				var opts = getSocialMessage(type);
				socialSharing[opts.method].apply(socialSharing, opts.params).then(shared, couldntShare);
			};
			this.init = function(place) {
				this.place = session.getTemp('place') || place;
				if (!this.place) {
					diag.show({
						title: 'Something\'s wrong',
						content: 'Something seems to be wrong , cant find the location you requested , please navigate back to the dashboard. :('
					});
					return;
				}

				this.refresh();
			};
		},
		Profile: function(backend, diag, loading, session, nav) {
			_self.viewmodels.Base.call(this, backend, loading, diag);
			var self = this;
			this.init = function(state) {
				var id;
				if (!state || !state.id) {
					id = backend.credentials.username;
				}
				this.invoke('getUserInfo', function(res) {
					self.info = res.responseObject;
				}, null, id || state.id);
			};

		},
		NotificationCenter: function(session, nav) {
			_self.viewmodels.Base.call(this);
			var self = this;
			this.init = function() {
				this.notifications = session.getPendingNotifications();
				if (!this.notifications.length)
					nav.back();
			};
			this.readNotification = function(notification) {
				session.readPendingNotification(notification);
			};
		},
		NewPlace: function(backend, diag, loading, session, nav, cam) {
			_self.viewmodels.Base.call(this, backend, loading, diag);
			var self = this;
			this.name = null;
			this.description = null;
			this.location = null;
			this.screenshot = null;
			this.address = null;
			this.init = function() {
				session.enableLocation(function(er, res) {
					if (er) {
						diag.show({
							title: 'Couldnt retrieve your current location'
						});
						self.location = null;
						return;
					}
					self.location = res;
					self.invoke('reverseGeocode', function(addr) {
						self.address = addr.display_name;
					}, null, res.longitude, res.latitude);
				});
			};
			this.openCamera = function() {
				cam.open().then(function(uri) {
					self.screenshot = uri;
				}, function(er) {
					diag.show({
						title: 'Couldnt open camera',
						content: er
					});
				});
			};
			this.save = function() {
				this.invoke('createNewPlace', function(r) {
					diag.show({
						title: 'successful',
						content: r.responseObject
					});
					nav.back();
				}, null, {
					name: self.name,
					description: self.description,
					address: self.address,
					longitude: self.location.longitude,
					latitude: self.location.latitude
				}, self.screenshot);
			};

		}
	};
	
	viewmodels.NewPlace.$args = ['backend', 'diag', 'loading', 'session', 'nav', 'cam'];
	viewmodels.Login.$args = ['backend', 'diag', 'nav', 'loading', 'browser', 'session'];
	viewmodels.Registration.$args = ['backend', 'nav', 'loading', 'diag'];
	viewmodels.ForgotPassword.$args = ['backend', 'nav', 'loading', 'diag'];
	viewmodels.Home.$args = ['backend', 'nav', 'loading', 'diag', 'session'];
	viewmodels.ClubRating.$args = ['backend', 'session', 'diag', 'loading', 'nav'];
	viewmodels.MessageBoard.$args = ['session'];
	viewmodels.Dashboard.$args = ['backend', 'loading', 'diag', 'nav', 'session'];
	viewmodels.PlaceDetail.$args = ['backend', 'diag', 'loading', 'session', 'nav', 'timer', 'socialSharing'];
	viewmodels.Profile.$args = ['backend', 'diag', 'loading', 'session', 'nav'];
	viewmodels.NotificationCenter.$args = ['session', 'nav'];
	viewmodels.SearchForPlace.$args = ['backend', 'session', 'diag', 'loading', 'nav'];
	viewmodels.ChangePassword.$args = ['backend', 'diag', 'nav', 'loading'];

	_self.inherit(_self.EventObject, viewmodels.Dashboard);
	_self.inherit(_self.EventObject, viewmodels.PlaceDetail);
	return viewmodels;
}