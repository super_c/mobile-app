function com_scheme_labs_411() {
	//create the main namespace for the entire application.
	_self = this;
	var utils = com_scheme_labs_common_utils();
	this.Promise = com_scheme_labs_promise();
	this.EventObject = com_scheme_labs_event_object();
	utils.extend(utils, this);
	utils.extend(this, utils);
	this.constants = {
		LOGON_TYPES: {
			GOOGLE: 'g+',
			NATIVE: 'native',
			GOOGLE_NATIVE:'g+native'
		},
		lookups: {
			PASSWORD_POLICY: 'PASSWORD_POLICY'
		},
		apps: {
			google_maps: 'google:maps'
		},
		paths: {
			new_place: '/home/new-place',
			login: '/login',
			change_password: '/home/change-password',
			dashboard: '/home/dashboard',
			registration: '/registration',
			club_rating: '/home/club-rating',
			message_board: '/home/message-board',
			place: '/home/place-detail',
			profile: '/home/account'

		},
		permissions: {
			location: 'location'
		},
		states: {
			place: {
				info: 'place-info',
				comments: 'place-comments'
			}
		},
		status: {
			success: '00',
			sessione_xpired: 'E5'
		},
		events: {
			notification: 'notification'
		},
		notifications: {
			full_message: '411-full-message',
			location: '411-location',
			pic: '411-location-pic'
		},
		templates: {
			user_maybe_in_location: 'templates/user-maybe-in-location.html',
			profile: 'templates/profile.html'
		},
		session: {
			USER_LOGOUT: 1,
			SESSION_EXPIRED: 2,
			BACK_BUTTON: 3,
			keys: {
				LOGON: 'LOGON'
			}
		}
	};
	utils.shims.ArrayRemoveItem();
	this.viewmodels = com_scheme_labs_viewmodels(this);
	this.backend = com_scheme_labs_backend(this);
}