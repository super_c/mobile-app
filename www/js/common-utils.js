function com_scheme_labs_common_utils() {
    var util = {
        inherit: function(superclass, childclass) {
            childclass.prototype = Object.create(superclass.prototype);
            childclass.prototype.constructor = childclass;
        },
        extend: function(original, extension) {

            for (var i in extension) {
                if (extension.hasOwnProperty(i)) {
                    original[i] = extension[i];
                }
            }
        },
        parseAmount: function(amt) {
            return amt ? parseFloat(parseFloat(amt.replace(',', '')).toFixed(2)) : 0;
        },
        getYYYYMMdd: function(d) {
            var yyyy = d.getFullYear().toString();
            var mm = (d.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = d.getDate().toString();
            return yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]); // padding
        },
        getQueryStringValue: function(url, key) {
            return unescape(url.replace(new RegExp("^(?:.*[&\\?]" + escape(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
        },
        parseUrl: function(url) {
            var a = document.createElement('a');
            a.href = url;
            return {
                host: a.hostname,
                path: a.path,
                port: a.port,
                origin: a.origin
            };
        },
        shims: {
            ArrayMax: function() {
                Array.prototype.max = function() {
                    return Math.max.apply(Math, this);
                };
            },
            ArrayMin: function() {
                Array.prototype.min = function() {
                    return Math.min.apply(Math, this);
                };
            },
            ArrayRemoveItem: function() {
                Array.prototype.removeItem = function(item) {
                    return this.indexOf(item) !== -1 ? this.splice(this.indexOf(item), 1) : null;
                };
            }
        },
        generateUUID: function() {
            var d = new Date().getTime();
            var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = (d + Math.random() * 16) % 16 | 0;
                d = Math.floor(d / 16);
                return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });
            return uuid;
        },
        container: function() {
            var _map = {};
            var SERVICE = 'CONTAINER_SERVICE';
            var SINGLETON = 'CONTAINER_INSTANCE';
            var STRIP_COMMENTS = /(\/\/.*$)|(\/\*[\s\S]*?\*\/)|(\s*=[^,\)]*(('(?:\\'|[^'\r\n])*')|("(?:\\"|[^"\r\n])*"))|(\s*=[^,\)]*))/mg;
            var ARGUMENT_NAMES = /([^\s,]+)/g;
            Function.prototype.construct = function(aArgs) {
                var fConstructor = this,
                    fNewConstr = function() {
                        fConstructor.apply(this, aArgs);
                    };
                fNewConstr.prototype = fConstructor.prototype;
                return new fNewConstr();
            };

            function getParamNames(func) {
                if (!func.$args) {
                    var fnStr = func.toString().replace(STRIP_COMMENTS, '');
                    var result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
                    if (result === null)
                        result = [];
                    return result;
                } else {
                    return func.$args;
                }
            }

            function remove(name) {
                delete _map[name];
            }

            function add(name, value, type) {
                _map[name] = {
                    type: type,
                    value: value
                };
            }

            function getItem(name) {
                var item = _map[name];
                if (item) {
                    if (item.type == SERVICE) {
                        var names = getParamNames(item.value);
                        var args = [];
                        for (var i = 0; i < names.length; i++) {
                            args.push(getItem(names[i]));
                        }
                        var object = item.value.construct(args);
                        return object;
                    }
                    if (item.type == SINGLETON) {
                        return item.value;
                    }
                } else {
                    throw 'There is nothing registered in the container with the name:' + name;
                }
            }
            var container = {
                add: function(name, service) {
                    var type = null;
                    if (typeof service == 'function') {
                        type = SERVICE;
                    }
                    if (typeof service == 'object') {
                        type = SINGLETON;
                    }
                    if (!type)
                        throw 'illegal argument service must either be an object or a function';

                    add(name, service, type);
                },
                resolve: function(name) {
                    return getItem(name);
                },
                inject: function(func) {
                    var tempName = util.generateUUID();
                    container.add(tempName, func);
                    var service = getItem(tempName);
                    remove(tempName);
                    return service;
                }
            };
            return container;
        },
        convertToNumbers: function(obj) {
            var result = {};
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    result[prop] = obj[prop] ? parseInt(obj[prop].replace(/,/g, '')) : 0;
                }
            }
            return result;
        },
        formatAmount: function(input) {
            if (input) {
                if (typeof input == 'number') {
                    input = '' + input;
                } else {
                    input = input.replace(/,/g, '');
                }
                var indexOfdot = null;
                var apply = input;
                var right = null;
                var index = 0;
                var result = '';
                if ((indexOfdot = input.indexOf('.')) !== -1) {
                    apply = input.slice(0, indexOfdot);
                    right = input.slice(indexOfdot, input.length - 1);
                }
                for (var i = apply.length - 1; i >= 0; i--) {
                    if (index == 3) {
                        result = ',' + result;
                        index = 0;
                    }
                    result = apply[i] + result;
                    index++;
                }

                if (right) {
                    result += right;
                }
                return result;
            }
            return input;
        }
    };
    return util;
}