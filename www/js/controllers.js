(function() {
	var genericController = function(modelName, skipInit,
		fn, lv) {
		return ['$scope', 'container', '$stateParams', function($scope, container, $stateParams) {
			$scope.$on("$ionicView.enter", function(event, data) {
				if (data.direction !== 'back' || !data.fromCache) {
					$scope.model = container.resolve(modelName);
					if ($scope.model.init && !skipInit) $scope.model.init($stateParams);
				}
				if (fn) fn(data, $scope.model);
			});
			$scope.$on("$ionicView.leave", function(event, data) {

				if (lv) lv(data);
			});
			$scope.$on('$destroy', function() {
				if ($scope.model && typeof($scope.model.destroy) == 'function')
					$scope.model.destroy();
			});

		}];
	};
	var refreshingController = function(modelName, skipInit, fn, lv) {
		var enterEvent;
		return ['$scope', 'container', '$stateParams', function($scope, container, $stateParams) {
			var ctrl = genericController(modelName, skipInit, function(data, model) {
				enterEvent = $scope.model.on('loaded', function() {
					$scope.$broadcast('scroll.refreshComplete');
				});
				if (fn) fn(data, model);
			}, function(data) {
				if (enterEvent) enterEvent();
				if (lv) lv(data);
			});

			ctrl[ctrl.length - 1]($scope, container, $stateParams);

		}];
	};
	angular.module('controllers', [])
		.controller('LoginCtrl', genericController('login'))
		.controller('HomeCtrl', genericController('home'))
		.controller('RegCtrl', genericController('registration'))
		.controller('ForgotPasswordCtrl', genericController('forgotPassword'))
		.controller('ChangePasswordCtrl',genericController('changePassword'))
		.controller('MessageBoardCtrl', genericController('messageBoard'))
		.controller('PlaceDetailCtrl', refreshingController('placeDetail'))
		.controller('NewPlaceCtrl', genericController('newPlace'))
		.controller('SearchForPlaceCtrl',genericController('searchForPlace'))
		.controller('ClubRatingCtrl', genericController('clubRating'))
		.controller('NotificationCenterCtrl', genericController('notificationCenter', false, function(data, model) {
			if (data.direction == 'back')
				model.init();
		}))
		.controller('ProfileCtrl', genericController('profile'))
		.controller('DashboardCtrl', ['$scope', 'container', '$ionicActionSheet', '$ionicScrollDelegate', function($scope, container, $ionicActionSheet, $ionicScrollDelegate) {
			var ctrl = refreshingController('dashboard', false, function() {
				$scope.ticket = $scope.model.on('loaded', function() {
					$ionicScrollDelegate.resize();
				});
			},function(){
				if($scope.ticket)
				$scope.ticket();
			});
			ctrl[ctrl.length - 1]($scope, container);
			var diag = container.resolve('diag');

			$scope.hideMenu = function(event) {
				if ($scope.popover) $scope.popover.close();
			};
			$scope.openMenu = function(event) {
				//incase i want to switch to a popover later.
				// $scope.popover = diag.popover({
				// 	name: 'dashboard',
				// 	content: $scope.model,
				// 	event: event
				// });
				var hideSheet = $ionicActionSheet.show({
					buttons: $scope.model.getActionButtons(),
					titleText: 'Choose an option',
					buttonClicked: function(index) {
						$scope.model.actionClicked(index);
						return true;
					}
				});
			};
		}]);
}());