function com_scheme_labs_event_object() {
    function EventObject() {
        console.log('Event object constructor invoked');
    }
    EventObject.prototype.invokeEvent = function(eveName, args) {
        var self = this;
        init.call(this);
        if (this.$$__events.hasOwnProperty(eveName)) {
            this.$$__events[eveName].forEach(function(item) {
                try {
                    item.cb.call(item.context || self, args);
                } catch (e) {
                    window.console.log(e);
                }

            });
        }
    };
    EventObject.prototype.on = function(eveName, cb, context) {
        init.call(this);
        var self = this;
        if (this.$$__events.hasOwnProperty(eveName)) {
            this.$$__events[eveName].push({
                context: context,
                cb: cb
            });
        } else {
            this.$$__events[eveName] = [{
                context: context,
                cb: cb
            }];
        }
        return (function() {
            self.detach(eveName, cb, context);
        }).bind(this);
    };
    EventObject.prototype.detach = function(eveName, cb, context) {
        init.call(this);
        var index = null;
        var self = this;
        if (this.$$__events.hasOwnProperty(eveName)) {
            this.$$__events[eveName].forEach(function(item) {
                if (item.cb == cb && item.context == context)
                    index = self.$$__events[eveName].indexOf(item);
            });
        }
        if (index)
            this.$$__events[eveName].splice(index, 1);
    };
    init = function() {
        if (!this.$$__events)
            this.$$__events = {};
    }
    return EventObject;
}