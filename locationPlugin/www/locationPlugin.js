var CLASS_NAME = 'LocationPlugin';
module.exports = {
	getLocation: function(args,successfulCB, failedCB) {
		cordova.exec(successfulCB, failedCB, CLASS_NAME, 'getLocation', args);
	},
	showDialog: function(successfulCB, failedCB) {

		cordova.exec(successfulCB, failedCB, CLASS_NAME, 'showDialog', []);
	},
	stopService: function(successfulCB, failedCB) {
		cordova.exec(successfulCB, failedCB, CLASS_NAME, 'stopService', []);
	}
}