package com.scheme.location.components;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.apache.cordova.PluginResult.Status;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.location.Location;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Pair;
import android.util.Log;

public class LocationPlugin extends CordovaPlugin {
	CallbackContext incomingMessageCallback = null;
	Activity main = null;
	ServiceConnection mConnection=null;
    public static String TAG="[LocationPlugin]";
	@Override
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		// TODO Auto-generated method stub
		super.initialize(cordova, webView);
		main = cordova.getActivity();

	}
@Override 
public void onDestroy () {
     doUnbindService();
};
	protected void doUnbindService() {
    if (mIsBound) {
        // If we have received the service, and hence registered with
        // it, then now is the time to unregister.
        if (mService != null) {
            try {
                Message msg = Message.obtain(null,
                        LocationService.MSG_UNREGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                // There is nothing special we need to do if the service
                // has crashed.
            }
        }

        // Detach our existing connection.
        main.unbindService(mConnection);
        mIsBound = false;
    }
}
	/** Messenger for communicating with service. */
	Messenger mService = null;
	/** Flag indicating whether we have called bind on the service. */
	boolean mIsBound;

	/**
	 * Handler of incoming messages from service.
	 */
	class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			Log.d(TAG,"Mesasge recieved by client");
			switch (msg.what) {
				case LocationService.MSG_SET_VALUE:
                  switch(msg.arg1){
					  case LocationService.MSG_GET_LOCATION:
						  try{
							  Log.d(TAG,"Casting object to Location and returning json result");
							  getLocation((Location) msg.obj, incomingMessageCallback);
							  Log.d(TAG,"successfully sent location");
						  }catch(Exception ex){

						  }

				  }
					break;
				default:
					super.handleMessage(msg);
			}
		}
	}

	/**
	 * Target we publish for clients to send messages to IncomingHandler.
	 */
	final Messenger mMessenger = new Messenger(new IncomingHandler());
public void getLocation(Location location,CallbackContext context){
	try{
                JSONObject item = new JSONObject();
                item.put("longitude", location.getLongitude());
                item.put("latitude",location.getLatitude());
                context.success(item);
	}catch(Exception ex){
           context.error("error occurred while getting location");
	}
}

	@Override
	public boolean execute(String action, JSONArray args,
			CallbackContext callbackContext) throws JSONException {
		// TODO Auto-generated method stub
		try{
		boolean skip=false;
		if (action != null) {
			if (action.equals("stopService")){
						main.stopService(new Intent(main, LocationService.class));
						Log.d(TAG,"-----------------------||  Unbinding service ||---------------------");
						doUnbindService();
						callbackContext.success();
						return true;
					}
			if (action.equals("getLocation")) {
				if(!LocationPlugin.this.mIsBound){

				main.startService(new Intent(main, LocationService.class));
				// use Application.ActivityLifecycleCallbacks to unbind service.
				main.bindService(new Intent(cordova.getActivity(),
						LocationService.class), mConnection=new ServiceConnection() {
                         CallbackContext cb=null;
                         String username="";
                         String auth_token="";
						public ServiceConnection setCB(CallbackContext cb,String username,String auth_token){
						   this.cb=cb;
						   this.username=username;
						   this.auth_token=auth_token;
                           return this;
						}
					@Override
					public void onServiceDisconnected(ComponentName name) {
						// TODO Auto-generated method stub
						mIsBound=false;
						mService=null;

					}

					@Override
					public void onServiceConnected(ComponentName name,
							IBinder service) {
						mService = new Messenger(service);
						try {
							mIsBound=true;
							incomingMessageCallback=cb;
							Message msg = Message.obtain(null,
									LocationService.MSG_REGISTER_CLIENT);
							msg.replyTo = mMessenger;
							mService.send(msg);

							// Give it some value as an example.
							msg = Message.obtain(null,
									LocationService.MSG_SET_VALUE, LocationService.MSG_GET_LOCATION, 0);
							        Bundle b=new Bundle();
							        b.putString("USERNAME",username);
                                    b.putString("AUTH_TOKEN",auth_token);
                                    msg.setData(b);
							mService.send(msg);
						} catch (RemoteException e) {
							// In this case the service has crashed before we could even
							// do anything with it; we can count on soon being
							// disconnected (and then reconnected if it can be restarted)
							// so there is no need to do anything here.
						}

					}
				}.setCB(callbackContext,args.getString(0),args.getString(1)), Activity.BIND_AUTO_CREATE);
                        PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT);
                        result.setKeepCallback(true);
                        callbackContext.sendPluginResult(result);
                        return true;
				}else{

					incomingMessageCallback=callbackContext;
					Message msg = Message.obtain(null,
							LocationService.MSG_SET_VALUE, LocationService.MSG_GET_LOCATION, 0);
			                        Bundle b=new Bundle();
							        b.putString("USERNAME",args.getString(0));
                                    b.putString("AUTH_TOKEN",args.getString(1));
                                    msg.setData(b);
					mService.send(msg);
					return true;
				}
			}

			return true;

		}


		//if(!skip)
		//callbackContext.sendPluginResult(new PluginResult(Status.NO_RESULT));

		}catch(Exception j){
		  Log.d(TAG,j.getMessage());
		  j.printStackTrace();
		  callbackContext.error("error occured while processing :"+j.getMessage());
		}
		return true;
	}
}
