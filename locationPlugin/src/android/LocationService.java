package com.scheme.location.components;


import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.Settings;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;

public class LocationService extends Service implements LocationListener{
    /** Keeps track of all current registered clients. */
    ArrayList<Messenger> mClients = new ArrayList<Messenger>();
    /** Holds last value set by a client. */
    int mValue = 0;
    /**
     * Command to the service to register a client, receiving callbacks
     * from the service.  The Message's replyTo field must be a Messenger of
     * the client where callbacks should be sent.
     */
    static final int MSG_REGISTER_CLIENT = 1;

    static final int MSG_GET_LOCATION=4;
    /**
     * Command to the service to unregister a client, ot stop receiving callbacks
     * from the service.  The Message's replyTo field must be a Messenger of
     * the client as previously given with MSG_REGISTER_CLIENT.
     */
    static final int MSG_UNREGISTER_CLIENT = 2;

    /**
     * Command to service to set a new value.  This can be sent to the
     * service to supply a new value, and will be sent by the service to
     * any registered clients with the new value.
     */
    static final int MSG_SET_VALUE = 3;
    private  Context mContext;

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    private final String DEBUG_TAG = "[GPS Upload]";

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    // Declaring a Location Manager
    protected LocationManager locationManager;

    public LocationService(){

    }
    /**
     * Handler of incoming messages from clients.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    break;
                case MSG_SET_VALUE:
                    for (int i=mClients.size()-1; i>=0; i--) {
                        try {
                            switch (mValue = msg.arg1) {
                                case MSG_GET_LOCATION:
                                    updateCredentials(msg.getData());
                                    mClients.get(i).send(Message.obtain(null,
                                            MSG_SET_VALUE, mValue, 0,getLocation()));
                                    if(location!=null && backend!=null)
                                            backend.updateLocation(location, new BackendAPI.Action(){
            @Override
            public void run(Boolean result,JSONObject response){
                if(result){
                    Log.d(DEBUG_TAG,"successfully uploaded new location");
                }else{
                    Log.d(DEBUG_TAG,"something went wrong while uploading location");
                }
                Log.d(DEBUG_TAG,"-------------------------------------handler done----------------------------------\n\n");
            }
        });
                                    break;
                            }

                        } catch (RemoteException e) {
                            // The client is dead.  Remove it from the list;
                            // we are going through the list from back to front
                            // so this is safe to do inside the loop.
                            mClients.remove(i);
                        }
                    }
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }
    final Messenger mMessenger = new Messenger(new IncomingHandler());
    private BackendAPI backend= null;

public void updateCredentials(android.os.Bundle b){
     android.content.SharedPreferences pref = mContext.getSharedPreferences(BackendAPI.PREF_NAME, 0);
     pref.edit().putString("USERNAME",b.getString("USERNAME")).putString("AUTH_TOKEN",b.getString("AUTH_TOKEN")).apply();
}
    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
                Log.d(DEBUG_TAG,"Nothing is enabled , cannot get location.");
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {

                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    Log.d("Network", "Network");
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                            locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     * */
    public void stopUsingGPS(){
        if(locationManager != null){
            Log.d(DEBUG_TAG,"Stoping Listeners for location updates");
            locationManager.removeUpdates(LocationService.this);
        }
    }

    /**
     * Function to get latitude
     * */
    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     * */
    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }

        // return longitude
        return longitude;
    }

    /**
     * Function to check GPS/wifi enabled
     * @return boolean
     * */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d(DEBUG_TAG,"\n\n-------------------Location changed handler called!!!!!!!----------------------\n\n");
        backend.updateLocation(location, new BackendAPI.Action(){
            @Override
            public void run(Boolean result,JSONObject response){
                if(result){
                    Log.d(DEBUG_TAG,"successfully uploaded new location");
                }else{
                    Log.d(DEBUG_TAG,"something went wrong while uploading location");
                }
                Log.d(DEBUG_TAG,"-------------------------------------handler done----------------------------------\n\n");
            }
        });
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();

    }

    @Override
    public IBinder onBind(Intent arg0) {
        return mMessenger.getBinder();
    }
    @Override
    public void onDestroy(){
        Log.d(DEBUG_TAG, "Destroy called!!");
        stopUsingGPS();
    }

    @Override
    public int onStartCommand(Intent i, int flags, int startId){
        Log.d(DEBUG_TAG,"start command fired, returning sticky");
        Log.d(DEBUG_TAG, "onStart");
        mContext=getApplicationContext();
        Log.d(DEBUG_TAG,"gotten application context... gettting location manager");
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Log.d(DEBUG_TAG, "gotten location manager");
        backend= BackendAPI.getInstance(mContext);
        Location location=getLocation();
        if(location!=null)
        Log.d(DEBUG_TAG,"Device current location:"+location.getLatitude()+","+location.getLongitude());
        else
        Log.d(DEBUG_TAG, "could not retrieve location");

        Log.d(DEBUG_TAG,"start command ending ....sending sticky now.");
        return START_STICKY;
    }
}
