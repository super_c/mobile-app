package com.scheme.location.components;

import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.Cache;
import com.android.volley.Request;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;

import android.util.Base64;
import android.util.Log;
import android.location.Location;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;


public class BackendAPI {

protected RequestQueue mRequestQueue;
protected SharedPreferences pref;
public static String PREF_NAME="The411Session";
protected String API_KEY="API_KEY";
protected String API_SECRET="API_SECRET";
protected String AUTH_TOKEN="AUTH_TOKEN";
protected String USERNAME="USERNAME";
protected String API_BASE_URL="API_BASE_URL";
public static String TAG="[BackendAPI]";
protected String api_key="";
protected String api_secret="";
protected String auth_token="";
protected String api_base_url="";
protected String username="";
private static BackendAPI _instance;
Context context;
private BackendAPI(Context context){

    this.context=context;
// Instantiate the cache
Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024); // 1MB cap

// Set up the network to use HttpURLConnection as the HTTP client.
Network network = new BasicNetwork(new HurlStack());

// Instantiate the RequestQueue with the cache and network.
mRequestQueue = new RequestQueue(cache, network);

// Start the queue
mRequestQueue.start();

pref = context.getSharedPreferences(PREF_NAME, 0);

}

private void getPrefs(){

api_key=pref.getString(API_KEY,"984b36d97bf84e6b589933e6fb171355289d0ad729c70cf33b7f8902786ef4db");
api_secret=pref.getString(API_SECRET,"a353d08b96355948c61954eec9a23bbda5b28edcd2f97276c25c48c8c21f661c");
api_base_url=pref.getString(API_BASE_URL,"http://10.0.2.2:4646/_geo/");
auth_token=pref.getString(AUTH_TOKEN,"");
//username=pref.getString(USERNAME,"");
}

public static BackendAPI getInstance(Context context){
    Log.d(TAG,"get instance called on BackendAPI");
    if(_instance==null) _instance=new BackendAPI(context);
   return _instance;
}

public void updateLocation(Location loc,Action action){
  String username=getUsername();
  String auth_token=getAuthToken();
  Log.d(TAG,"username:"+username);
  Log.d(TAG,"token:"+auth_token);
  if(username!=null&& auth_token!=null){
    Log.d(TAG,"creating updateLocation command params...");
    HashMap<String,String> map=new HashMap<String,String>();
    map.put("latitude",""+loc.getLatitude());
    map.put("longitude",""+loc.getLongitude());
    map.put("username",pref.getString(USERNAME,""));
    Log.d(TAG,"params created successfully");
    new UpdateLocationCommand(map).execute(action);
  }
}
private String getAuthToken(){
    return pref!= null ? pref.getString(AUTH_TOKEN,null): null;
}
private String getUsername(){
    return pref !=null ? pref.getString(USERNAME,null):null;
}
public interface Action{
      void run(Boolean isOk,JSONObject response);
}
protected class UpdateLocationCommand extends Command {
 protected UpdateLocationCommand(HashMap<String,String> data){
    super("update-location",Request.Method.POST,true,data);
 }
}
protected class Command
{
    String resource="";
    int method;
    Boolean requiresAuth=false;
    HashMap<String,String> data=null;
    ArrayList<String> baseList=new ArrayList<String>();
      protected Command(String resource,int method,Boolean requiresAuth, HashMap<String,String> data){
        this.resource=resource;
        this.method=method;
        this.requiresAuth=requiresAuth;
        this.data=data;
        setupDefaultBaseList();
      }
      private String getNonce(){
         return ""+System.currentTimeMillis();
      }
      private void setupDefaultBaseList(){
        baseList.clear();
        baseList.add("username");
        baseList.add("baseUrl");
        baseList.add("timestamp");
        baseList.add("applicationKey");
        baseList.add("params");
        baseList.add("token");
        baseList.add("nonce");
      }
      private String getTimestamp(){
            return ""+System.currentTimeMillis();
      }
      protected String sign(){

               String username =this.data.get("username")!=null ? this.data.get("username"): BackendAPI.this.username;
               StringBuilder  signatureBaseString = new StringBuilder("");
               String appKey = api_key;
               String baseUrl = api_base_url+resource;
               String nonce = getNonce();
               String  timestamp = getTimestamp();
               ArrayList<String> parameters = new ArrayList<String>();
            String header = "username=" + EncodingUtil.encodeURIComponent(username) + ";applicationKey=" + EncodingUtil.encodeURIComponent(appKey) + ";baseUrl=" + EncodingUtil.encodeURIComponent(baseUrl) + ";nonce=" + EncodingUtil.encodeURIComponent(nonce) + ";timestamp=" + EncodingUtil.encodeURIComponent(timestamp) + ";cipher=HMACSHA1";
            ArrayList baseList = this.baseList;
          Collections.sort(baseList, new Comparator<String>() {
              @Override
              public int compare(String s1, String s2) {
                  return s1.compareToIgnoreCase(s2);
              }
          });
            for (int o = 0; o < baseList.size(); o++) {
                String ca=baseList.get(o).toString();
                if (ca.equals("username")) {
                    signatureBaseString.append(username);

                } else if (ca.equals("timestamp")) {
                    signatureBaseString.append(timestamp);

                } else if (ca.equals("baseUrl")) {
                    signatureBaseString.append(baseUrl);

                } else if (ca.equals("nonce")) {
                    signatureBaseString.append(nonce);

                } else if (ca.equals("token")) {
                    signatureBaseString.append(BackendAPI.this.getAuthToken());

                } else if (ca.equals("applicationKey")) {
                    signatureBaseString.append(appKey);

                } else if (ca.equals("params")) {
                    header += ";params:";
                    String paramsHeader = "";
                    ArrayList params = new ArrayList();
                    for (String prop : this.data.keySet()) {
                        params.add(prop);
                        paramsHeader += prop + "=" + EncodingUtil.encodeURIComponent(this.data.get(prop));
                        paramsHeader += ",";
                    }
                    try {
                        StringBuilder b = new StringBuilder(paramsHeader);
                        b.replace(paramsHeader.lastIndexOf(","), paramsHeader.lastIndexOf(",") + 1, "");
                        paramsHeader = b.toString();
                    } catch (Exception e) {
                        Log.v(TAG, "Error occurred while sending info to the 411");
                    }
                    header += EncodingUtil.encodeURIComponent(paramsHeader);
                    Collections.sort(params, new Comparator<String>() {
                        @Override
                        public int compare(String s1, String s2) {
                            return s1.compareToIgnoreCase(s2);
                        }
                    });
                    for (int i = 0; i < params.size(); i++) {
                        signatureBaseString.append(params.get(i)).append(this.data.get(params.get(i)));
                    }

                }
            }
              String base_string = signatureBaseString.toString();
              String key = api_secret;
        try {
            Mac mac = Mac.getInstance("HmacSHA1");
            SecretKeySpec secret = new SecretKeySpec(key.getBytes("UTF-8"), mac.getAlgorithm());
            mac.init(secret);
            byte[] digest = mac.doFinal(base_string.getBytes());

            // Base 64 Encode the results
            //byte[] data = text.getBytes("UTF-8");
            // String base64 = Base64.encodeToString(data, Base64.DEFAULT);
            String retVal = Base64.encodeToString(digest, Base64.NO_WRAP);
            Log.v(BackendAPI.this.TAG, "String: " + base_string);
            Log.v(BackendAPI.this.TAG, "key: " + key);
            Log.v(BackendAPI.this.TAG, "result: " + retVal);
            header += ";signature=" + EncodingUtil.encodeURIComponent(retVal);
            Log.d(TAG,"Authorization:"+header);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

         return header;
      }
      protected void execute(final Action responseHandler){
        getPrefs();
        String url=api_base_url+resource;
        JSONObject body=null;
        switch(this.method){
            case Request.Method.GET:
            break;
            case Request.Method.POST:
                body=new JSONObject(data);
                Log.d(TAG,body.toString());
            break;
        }

    JsonObjectRequest jsObjRequest = new JsonObjectRequest
        (this.method, url, body, new Response.Listener<JSONObject>() {

    @Override
    public void onResponse(JSONObject response) {
        Log.d(TAG,"Response from remote server: " + response.toString());
        responseHandler.run(true, response);
    }
}, new Response.ErrorListener() {

    @Override
    public void onErrorResponse(VolleyError error) {
        // TODO Auto-generated method stub
          Log.d(TAG,"Error from remote server: " + error.getMessage());
          responseHandler.run(false, null);
    }
}){
        @Override
         public Map<String, String> getHeaders() throws AuthFailureError {
           HashMap<String, String> headers = new HashMap<String, String>();
           if(Command.this.requiresAuth)
           headers.put("Authorization", Command.this.sign());

           return headers;
       }
        };
        Log.d(TAG,"Adding new request to queue");
        mRequestQueue.add(jsObjRequest);

      }
}

}
